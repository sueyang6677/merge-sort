import numpy as np
import time
import merge_sort

length=[1000,2000,4000,8000,16000,32000,64000,128000]
time_average=[]

for n in length:
    list_average=np.random.randint(1,n+1,n)
    list_best=np.sort(list_average)
    list_worst=np.sort(list_average)[::-1]
    
    start=time.time()
    merge_sort.merge_sort(list_average)
    end=time.time()
    time_average.append(end-start)
    
print(time_average)