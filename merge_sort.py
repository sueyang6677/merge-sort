import numpy as np
import time

def merge_sort(list_):
    if len(list_)<=1:
        return list_
    
    a=len(list_)//2
    left=merge_sort(list_[:a])
    right=merge_sort(list_[a:])
    
    return reduce(left,right)

def reduce(left,right):
    result=[]
    l_index=0
    r_index=0
    
    while True:
        if left[l_index]<right[r_index]:
            result.append(left[l_index])
            l_index=l_index+1
        else:
            result.append(right[r_index])
            r_index=r_index+1
        if l_index==len(left):
            result.extend(right[r_index:])
            break
        if r_index==len(right):
            result.extend(left[l_index:])
            break
        
    return result